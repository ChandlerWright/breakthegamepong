﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PongBallMove : MonoBehaviour
{
    public Score points;
    public Transform ballStart;
    public Audio_Manager audioManager;
    public AudioClip leftPaddle;
    public AudioClip rightPaddle;
    public AudioClip miss;
    public AudioClip powerUp;

    public int pointTracker;
    public int pointsToAdd = 100;

    public float speed = 5;

    public bool isPaused = false;

    void Start()
    {
        points = GameObject.FindWithTag("ball").GetComponent<Score>();

        audioManager = GameObject.FindWithTag("AudioManager").GetComponent<Audio_Manager>();

        MoveStart();

        InvokeRepeating("StillMoving", 5f, 5f);
        
    }

    void MoveStart()
    {
        transform.position = ballStart.position;

        GetComponent<Rigidbody>().velocity = new Vector2(0f, 0f);

        StartCoroutine(MoveDelay(1.5f));
    }

    /*
     * Code for calculating the trajectory of the ball based on whether
     * it hits the top, center or bottom of the panel was used from
     * http://noobtuts.com/
     * Some variable names changed to make more sense within the context
     * of this particular project. */
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.CompareTag("Left"))
        {
            //store a float value of the y value returned from bounceVariance
            //we pass the current ball position, use the col variable which holds the collision
            //information (in this case, the paddle we hit) to get the paddle's position, and
            //then dig further into the collision info to get the paddle's height via its bounding box size Y
            float paddleY = bounceVariance(transform.position, col.transform.position, col.collider.bounds.size.y);
            //now take the y value we calculated in bounceVariance and store it in a new Vector2 object,
            //setting the x value +- depending on which paddle we hit (left paddle sends the ball +x (right), 
            //right panel sends the ball -x (left)
            Vector2 newDirection = new Vector2(1, paddleY).normalized; //normalize the vector so its length = 1 so we can still multiply by our ball speed
            //finally, add velocity in the direction of the new normalized vector, multiplying by our speed
            GetComponent<Rigidbody>().velocity = newDirection * speed;
            pointTracker = 0;
            audioManager.PlaySFX(leftPaddle);
        }

        //same as above, but for the opposite paddle, meaning the only change is the x direction of the vector
        //(-x to the left instead of +x to the right)
        if (col.gameObject.CompareTag("Right"))
        {
            float paddleY = bounceVariance(transform.position, col.transform.position, col.collider.bounds.size.y);
            Vector2 newDirection = new Vector2(-1, paddleY).normalized;
            GetComponent<Rigidbody>().velocity = newDirection * speed;
            pointTracker = 1;
            audioManager.PlaySFX(rightPaddle);
        }

        if(col.gameObject.CompareTag("TopWall") && transform.position.x < 0)
        {
            float paddleX = bounceVariance(transform.position, col.transform.position, col.collider.bounds.size.y);
            Vector2 newDirection = new Vector2(paddleX, -1).normalized;
            GetComponent<Rigidbody>().velocity = newDirection * speed;
        }

        if (col.gameObject.CompareTag("TopWall") && transform.position.x >= 0)
        {
            float paddleX = bounceVariance(transform.position, col.transform.position, col.collider.bounds.size.y);
            Vector2 newDirection = new Vector2(paddleX, 1).normalized;
            GetComponent<Rigidbody>().velocity = newDirection * speed;
        }

        if (col.gameObject.CompareTag("BottomWall") && transform.position.x < 0)
        {
            float paddleX = bounceVariance(transform.position, col.transform.position, col.collider.bounds.size.y);
            Vector2 newDirection = new Vector2(paddleX, -1).normalized;
            GetComponent<Rigidbody>().velocity = newDirection * speed;
        }

        if (col.gameObject.CompareTag("BottomWall") && transform.position.x >= 0)
        {
            float paddleX = bounceVariance(transform.position, col.transform.position, col.collider.bounds.size.y);
            Vector2 newDirection = new Vector2(paddleX, 1).normalized;
            GetComponent<Rigidbody>().velocity = newDirection * speed;
        }

        if (col.gameObject.CompareTag("LeftWall"))
        {
            audioManager.PlaySFX(miss);
            MoveStart();
        }

        if (col.gameObject.CompareTag("RightWall"))
        {
            audioManager.PlaySFX(miss);
            MoveStart();
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("PowerUp") || col.gameObject.CompareTag("DoublePoints"))
        {
            audioManager.PlaySFX(powerUp);
        }
    }

    /*
     * This method was created with help from http://noobtuts.com/ to calculate
     * the trajectory of the ball with some simple vector math
     * Some variable names changed to make more sense within the context
     * of this particular project.*/
     //bounceVariance will take the parameters of the pong ball's current position, the position of
     //the paddle that it hits, and the height of the paddle it hits 
    float bounceVariance(Vector2 ballPos, Vector2 paddlePos, float paddleHeight)
    {
        //return a float value of the y vector of the pong ball subtracted by
        //the y vector of the paddle's position, divided by the height
        //of the paddle to get where we are in relation to the paddle
        return (ballPos.y - paddlePos.y) / paddleHeight;
    }

    IEnumerator MoveDelay(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        float startDirectionDecider = Random.value;

        if (startDirectionDecider < 0.5f)
        {
            GetComponent<Rigidbody>().velocity = Vector2.left * speed;
        }
        else
        {
            GetComponent<Rigidbody>().velocity = Vector2.right * speed;
        }
    }

    void StillMoving()
    {
        Vector3 areWeMoving = new Vector3(0f, 0f, 0f);
        if(GetComponent<Rigidbody>().velocity  == areWeMoving)
        {
            MoveStart();
        }
    }
}
