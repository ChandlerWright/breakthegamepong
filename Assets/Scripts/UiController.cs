﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class UiController : MonoBehaviour
{
    public void _button(int scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void _quitButton()
    {
        Application.Quit();
    }

}
